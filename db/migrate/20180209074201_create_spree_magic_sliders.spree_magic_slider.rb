# This migration comes from spree_magic_slider (originally 20180208130340)
class CreateSpreeMagicSliders < ActiveRecord::Migration[5.1]
  def change
    create_table :spree_magic_sliders do |t|

      t.timestamps
    end
  end
end
