# This migration comes from spree_top_bar_notification (originally 20180201065229)
class CreateSpreeTopbars < ActiveRecord::Migration[5.1]
  def up
    create_table :spree_topbars do |t|
    	t.text :message
    	t.string :color
    	t.string :background_color
    	t.timestamps
    end
  end
  def down
  	drop_table :spree_topbars
  end
end
