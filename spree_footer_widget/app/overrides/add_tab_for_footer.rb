Deface::Override.new(
  virtual_path: 'spree/layouts/admin',
  name: 'footer_widget',
  insert_bottom: '#main-sidebar',
  partial: 'spree/admin/shared/footer_widget_menu'
)