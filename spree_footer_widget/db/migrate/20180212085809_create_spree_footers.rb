class CreateSpreeFooters < ActiveRecord::Migration[5.1]
  def up
    create_table :spree_footers do |t|
      t.text :content
      t.timestamps
    end

  end

  def down
  	drop_table :content
  end
end
