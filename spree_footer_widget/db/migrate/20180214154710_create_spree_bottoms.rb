class CreateSpreeBottoms < ActiveRecord::Migration[5.1]
  def up
    create_table :spree_bottoms do |t|
      t.text :leftCol
      t.text :middleCol
      t.text :rightCol



      t.timestamps
    end
  end

  def down
     drop_table :spree_bottoms
  end
end
